import express from "express";

console.log(express);


var express = require('express');
var app = express();

app.use(express.json());
app.use('/api', require('./src/routes'));

var port = process.env.PORT || 3002;

app.listen(port, function () {
  console.log('Umbler creative-bank on port %s', port);
});
