const requireDir = require('require-dir');
const moment = require('moment');
const format = requireDir('./../../src/').format;

const today = moment().format("YYYY-MM-DD")
const age = 18;

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('user', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                is: {
                    args: ["^[a-záàâãéèêíïóôõöúçñ ]+$", 'i'],
                    msg: 'Somente texto.',
                },
                notEmpty: {
                    args: false,
                    msg: 'Por favor, digita o primeiro nome.'
                },
                notNull: {
                    msg: 'Por favor, digita o primeiro nome.'
                },
                len: {
                    args: [0, 255],
                    msg: 'máximo 255 caracteres'
                },
            }
        },
        lastName: {
            type: DataTypes.STRING,
            validate: {
                is: {
                    args: ["^[a-záàâãéèêíïóôõöúçñ ]+$", 'i'],
                    msg: 'Somente texto.',
                },
                len: {
                    args: [0, 255],
                    msg: 'máximo 255 caracteres'
                },
            }
        },
        dateOfBirth: {
            type: DataTypes.DATE,
            validate: {
                isDate: {
                    args: true,
                    msg: 'Por favor, informe uma data nesse formato YYYY-MM-DD'
                },
                isBefore: {
                    args: [format.subtractYear(today, age)],
                    msg: `O usuário deve ter idade minima de ${age} anos.`,
                }
            }
        },
        cep: {
            type: 'CHAR(8)',
            allowNull: false,
            validate: {
                is: {
                    args: ['^[0-9]+$'],
                    msg: 'Somente número.',
                },
                notNull: {
                    msg: 'Por favor, informa o cep.'
                },
                len: {
                    args: [8, 8],
                    msg: 'Por favor, digita um cep com 8 números.'
                },
            },
        },
        cpf: {
            type: 'CHAR(11)',
            unique: {
                msg: 'Esse cpf já foi cadastrado no sistema.',
            },
            validate: {
                is: {
                    args: ['[0-9]+$'],
                    msg: 'Somente número.',
                },
                len: {
                    args: [11, 11],
                    msg: 'Por favor, digita um cpf com 11 números.'
                },
                isCpfValido(value) {
                    function testaCPF(cpf) {
                        let soma = 0;
                        let resto = 0;
                        if (cpf == "00000000000") return false;
                        for (i = 1; i <= 9; i++) soma = soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
                        resto = (soma * 10) % 11;
                        if ((resto == 10) || (resto == 11)) resto = 0;

                        if (resto != parseInt(cpf.substring(9, 10))) return false;
                        soma = 0;
                        for (i = 1; i <= 10; i++) soma = soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
                        resto = (soma * 10) % 11;

                        if ((resto == 10) || (resto == 11)) resto = 0;
                        if (resto != parseInt(cpf.substring(10, 11))) return false;
                        return true;
                    }

                    if (!testaCPF(value)) {
                        throw new Error('Por favor, digita um cpf válido.');
                    }
                }
            }
        },
        gender: {
            type: DataTypes.INTEGER,
            allowNull: false,
            isNumeric: true,
            validate: {
                isIn: {
                    args: [[1, 2, 3, '1', '2', '3']],
                    msg: 'Por favor, selecione uma opção válida.',
                }
            }
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                is: {
                    args: ['^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'],
                    msg: 'Por favor, informe um email válido'
                },
                notNull: {
                    msg: 'Por favor, informa o email.'
                },
                len: {
                    args: [0, 255],
                    msg: 'máximo 255 caracteres'
                },
            },
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'Por favor, informa senha.'
                },
            },
        },
    }, {
        freezeTableName: true,
        timestamps: false
    });

    return User;
};
