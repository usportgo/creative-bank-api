require('dotenv/config');

module.exports = {
  connectionLimit: 100,
  host: process.env.DB_HOST,
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DATABASE,
  dialect: 'mysql',
  port: 3306,
}
