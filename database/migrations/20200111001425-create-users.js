'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .sequelize
      .query(
        `CREATE TABLE IF NOT EXISTS form (
          id INT(11) NOT NULL AUTO_INCREMENT,
          user_id INT(11) NOT NULL,
          theme_id INT(11) NOT NULL,
          name VARCHAR(45) NOT NULL,
          PRIMARY KEY (id),
          UNIQUE INDEX id_UNIQUE (id ASC),
          INDEX fk_form_user_idx (user_id ASC),
          INDEX fk_form_theme1_idx (theme_id ASC),
          CONSTRAINT fk_form_user
            FOREIGN KEY (user_id)
            REFERENCES user (id)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT fk_form_theme1
            FOREIGN KEY (theme_id)
            REFERENCES theme (id)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB
        DEFAULT CHARACTER SET = utf8;`
      );
  },

  down: (queryInterface, Sequelize) => {}
};
