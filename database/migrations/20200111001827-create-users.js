'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .sequelize
      .query(
        `CREATE TABLE IF NOT EXISTS form_version (
          id INT(11) NOT NULL AUTO_INCREMENT,
          form_id INT(11) NOT NULL,
          number VARCHAR(45) NOT NULL,
          date VARCHAR(45) NOT NULL,
          PRIMARY KEY (id),
          UNIQUE INDEX id_UNIQUE (id ASC),
          INDEX fk_form_version_form1_idx (form_id ASC),
          CONSTRAINT fk_form_version_form1
            FOREIGN KEY (form_id)
            REFERENCES form (id)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB
        DEFAULT CHARACTER SET = utf8;`
      );
  },

  down: (queryInterface, Sequelize) => {}
};
