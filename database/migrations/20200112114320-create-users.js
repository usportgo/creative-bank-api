'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .sequelize
    .query(
      `ALTER TABLE form 
      ADD INDEX fk_form_user_idx (user_id ASC),
      ADD INDEX fk_form_theme1_idx (theme_id ASC),
      DROP INDEX fk_form_theme1_idx ,
      DROP INDEX fk_form_user_idx ;
      `
      );
  },

  down: (queryInterface, Sequelize) => {}
};
