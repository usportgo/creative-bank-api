'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .sequelize
    .query(
      `
      CREATE TABLE IF NOT EXISTS form_field_option (
        id INT(11) NOT NULL AUTO_INCREMENT,
        form_field_id INT(11) NOT NULL,
        name VARCHAR(45) NOT NULL,
        PRIMARY KEY (id),
        UNIQUE INDEX id_UNIQUE (id ASC),
        INDEX fk_form_field_option_form_field1_idx (form_field_id ASC),
        CONSTRAINT fk_form_field_option_form_field1
          FOREIGN KEY (form_field_id)
          REFERENCES form_field (id)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION)
      ENGINE = InnoDB
      DEFAULT CHARACTER SET = utf8;
      `
      );
  },

  down: (queryInterface, Sequelize) => {}
};
