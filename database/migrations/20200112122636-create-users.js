'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .sequelize
    .query(
      `
      CREATE TABLE IF NOT EXISTS answer (
        id INT(11) NOT NULL AUTO_INCREMENT,
        form_id INT(11) NOT NULL,
        user_id INT(11) NOT NULL,
        form_field_id INT(11) NULL DEFAULT NULL,
        name VARCHAR(45) NULL DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE INDEX id_UNIQUE (id ASC),
        INDEX fk_answer_form1_idx (form_id ASC),
        INDEX fk_answer_user1_idx (user_id ASC),
        INDEX fk_answer_form_field1_idx (form_field_id ASC),
        CONSTRAINT fk_answer_form1
          FOREIGN KEY (form_id)
          REFERENCES form (id)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION,
        CONSTRAINT fk_answer_user1
          FOREIGN KEY (user_id)
          REFERENCES user (id)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION,
        CONSTRAINT fk_answer_form_field1
          FOREIGN KEY (form_field_id)
          REFERENCES form_field (id)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION)
      ENGINE = InnoDB
      DEFAULT CHARACTER SET = utf8;
      `
      );
  },

  down: (queryInterface, Sequelize) => {}
};
