'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .sequelize
    .query(
      `
      ALTER TABLE form_version 
      ADD CONSTRAINT fk_form_version_form1
        FOREIGN KEY (form_id)
        REFERENCES form (id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION;
      `
      );
  },

  down: (queryInterface, Sequelize) => {}
};
