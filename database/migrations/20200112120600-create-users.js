'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .sequelize
    .query(
      `
      ALTER TABLE form_version 
      ADD INDEX fk_form_version_form1_idx (form_id ASC),
      DROP INDEX fk_form_version_form1_idx ;
      `
      );
  },

  down: (queryInterface, Sequelize) => {}
};
