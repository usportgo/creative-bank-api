'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .sequelize
    .query(
      `ALTER TABLE user 
        CHANGE COLUMN name name VARCHAR(255) NOT NULL ,
        CHANGE COLUMN lastName lastName VARCHAR(255) NULL DEFAULT NULL ,
        CHANGE COLUMN cep cep CHAR(8) NOT NULL ,
        CHANGE COLUMN cpf cpf CHAR(11) NOT NULL ,
        CHANGE COLUMN email email VARCHAR(255) NOT NULL ,
        CHANGE COLUMN password password VARCHAR(255) NOT NULL ;`
      );
  },

  down: (queryInterface, Sequelize) => {}
};
