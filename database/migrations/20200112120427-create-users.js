'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .sequelize
    .query(
      `
      ALTER TABLE form_version 
      DROP FOREIGN KEY fk_form_version_form1;
      `
      );
  },

  down: (queryInterface, Sequelize) => {}
};
