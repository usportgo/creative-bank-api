'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .sequelize
    .query(
      `
      ALTER TABLE form ADD CONSTRAINT fk_form_user
        FOREIGN KEY (user_id)
        REFERENCES user (id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      ADD CONSTRAINT fk_form_theme1
        FOREIGN KEY (theme_id)
        REFERENCES theme (id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION;
      `
      );
  },

  down: (queryInterface, Sequelize) => {}
};
