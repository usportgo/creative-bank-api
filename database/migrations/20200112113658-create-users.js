'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .sequelize
    .query(
      `ALTER TABLE form 
      DROP FOREIGN KEY fk_form_theme1;
      `
      );
  },

  down: (queryInterface, Sequelize) => {}
};
