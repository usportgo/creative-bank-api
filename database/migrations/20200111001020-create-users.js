'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .sequelize
      .query(
        `CREATE TABLE IF NOT EXISTS theme (
          id INT(11) NOT NULL AUTO_INCREMENT,
          name VARCHAR(45) NOT NULL,
          PRIMARY KEY (id),
          UNIQUE INDEX id_UNIQUE (id ASC),
          UNIQUE INDEX name_UNIQUE (name ASC))
        ENGINE = InnoDB
        DEFAULT CHARACTER SET = utf8;`
      );
  },

  down: (queryInterface, Sequelize) => {}
};
