'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user', [{
      id: undefined,
      name: 'DEV',
      lastName: 'CREATIVE BANK',
      dateOfBirth: new Date("2019-09-30"),
      cep: '30640070',
      cpf: '31951957040',
      gender: 1,
      email: 'dev@creativebank.com',
      password: '123456'
    }]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user', {
      id: 1,
    })
  }
};
