import moment from "moment";

export function dateBr(date: string) {
  return moment(date, ["YYYY-MM-DD", "DD/MM/YYYY"]).format("YYYY-MM-DD");
};

export function subtractYear(date: string, year: number) {
  return moment(date, ["YYYY-MM-DD", "DD/MM/YYYY"]).subtract(year, "years").format("YYYY-MM-DD");
};
