import express from "express";
import routes from "./routes";

const app = express();

app.use(express.json());
app.use("/api", routes);

const port = process.env.PORT || 3002;

app.listen(port, function() {
  console.log("Umbler creative-bank on port %s", port);
});
