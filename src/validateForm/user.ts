import * as jwtPassword from "../token/password";
import { dateBr } from "../format";

interface IUser {
    id: any,
    name: string;
    lastName: string;
    dateOfBirth: string;
    cep: string;
    cpf: string;
    gender: number;
    email: string;
    password: string;
    confirmPassword: string;
}

export function validate({
    id,
    name,
    lastName,
    dateOfBirth,
    cep,
    cpf,
    gender,
    email,
    password,
    confirmPassword,
  }: IUser) {
  const fieldsError = [];

  if (!name) {
    fieldsError.push({
      field: "name",
      message: "Campo obrigatórios.",
    });
  }

  if (!lastName) {
    fieldsError.push({
      field: "lastName",
      message: "Campo obrigatórios.",
    });
  }

  if (!dateOfBirth) {
    fieldsError.push({
      field: "dateOfBirth",
      message: "Campo obrigatórios.",
    });
  }

  if (!cep) {
    fieldsError.push({
      field: "cep",
      message: "Campo obrigatórios.",
    });
  }

  if (!cpf) {
    fieldsError.push({
      field: "cpf",
      message: "Campo obrigatórios.",
    });
  }

  if (!gender) {
    fieldsError.push({
      field: "gender",
      message: "Campo obrigatórios.",
    });
  }

  if (!email) {
    fieldsError.push({
      field: "email",
      message: "Campo obrigatórios.",
    });
  }

  if (!password) {
    fieldsError.push({
      field: "password",
      message: "Campo obrigatórios.",
    });
  }

  const passwordIsValid = password
    && /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/
    .test(password);

  if (!passwordIsValid) {
    fieldsError.push({
      field: "password",
      message: "Por favor, informa uma senha com números e Letras maiúsculas e minusculas.",
    });
  }

  if (!confirmPassword) {
    fieldsError.push({
      field: "confirmPassword",
      message: "Campo obrigatórios.",
    });
  }

  if (confirmPassword && password) {
    if (confirmPassword !== password) {
      fieldsError.push({
        field: "confirmPassword",
        message: "Por favor, informa a mesma senha.",
      });
    }
  }

  if (fieldsError.length > 0) {
    return {
      isValid: false,
      fieldsError,
    };
  }

  return {
    isValid: true,
    field: {
      id,
      name,
      lastName,
      dateOfBirth: dateBr(dateOfBirth),
      cep,
      cpf,
      gender,
      email,
      password: jwtPassword.token(password),
    }
  };
};