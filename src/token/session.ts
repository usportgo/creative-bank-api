import * as dotenv from "dotenv";
import * as JWT from "jsonwebtoken";

interface IUser {
    id: number;
    name: string;
    lastName: string;
    dateOfBirth: string;
    cep: string;
    cpf: string;
    gender: number;
    email: string;
}

export function session({
    id,
    name,
    lastName,
    dateOfBirth,
    cep,
    cpf,
    gender,
    email,
}: IUser) {
    return JWT.sign(
        {
            id: id,
            name: name,
            lastName: lastName,
            dateOfBirth: dateOfBirth,
            cep: cep,
            cpf: cpf,
            gender: gender,
            email: email,
        },
        process.env.JWT_PASSWORD,
        { expiresIn: '2h' }
    );
}
