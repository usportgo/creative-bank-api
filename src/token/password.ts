import * as dotenv from "dotenv";
import * as JWT from "jsonwebtoken";

export function token(password: string) {
  return JWT
    .sign(
      password,
      process.env.JWT_PASSWORD
    );
}
