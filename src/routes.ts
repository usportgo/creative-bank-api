import express from "express";
import * as UserController from "./Controllers/UserController";

const routes = express.Router();


routes.get('/user/:id', UserController.index);
routes.post('/user', UserController.create);
routes.put('/user/:id', UserController.update);
routes.post('/login', UserController.login);

export default routes;
