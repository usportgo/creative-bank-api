import requireDir from "require-dir";
import { validate } from "../validateForm/user";
import { dateBr } from "../format";
import * as jwtAuthenticated from "../token/authenticated";
import * as jwtSession from "../token/session";

const entity = requireDir("./../../app/models", {
  mapKey(value:any, baseName:any) {
    return baseName.toUpperCase();
  }
});

const User = entity.INDEX.sequelize.models.user;

export async function index(req: any, res: any) {
  const { id } = req.params;

  try {
    const user = await User
      .findOne({ where: {id},attributes:
        [
          'id',
          'name',
          'lastName',
          'dateOfBirth',
          'cep',
          'cpf',
          'gender',
          'email',
        ]
      });

    return res
      .status(200)
      .json(user.dataValues);
  } catch (error) { 
    return res
      .status(422)
      .send({ error: "Houve um error." });
  }
};

export async function create(req: any, res: any) {
  const form = validate(req.body);

  if (!form.isValid) {
    return res
      .status(422)
      .json(form.fieldsError);
  }

  try {
    const newUser = await User.create(form.field);
    return res
      .status(200)
      .json({
        id: newUser.id,
        name: newUser.name,
        lastName: newUser.lastName,
        dateOfBirth: newUser.dateOfBirth,
        cep: newUser.cep,
        cpf: newUser.cpf,
        gender: newUser.gender,
        email: newUser.email,
      });
  } catch (error) {
    return res
      .status(422)
      .send({ error: "Houve um error." });
  }
};

export async function update(req: any, res: any) {
  const form = req.body;
  const { id } = req.params;

  if (form.id.toString() !== id.toString()) {
    res
      .status(422)
      .json({
        error: "inválido"
      });
  }

  try {
    const user = await User
    .findOne({ where: {id},attributes:
      [
        'id',
        'name',
        'lastName',
        'dateOfBirth',
        'cep',
        'cpf',
        'gender',
        'email',
      ]
    });
  
    if (!user) {
      return res
        .status(404)
        .send({ error: "Usuário não encontrado." });
    }

    await user.update(Object.assign({}, user, {
      name: form.name,
      lastName: form.lastName,
      dateOfBirth: dateBr(form.dateOfBirth),
      cep: form.cep,
      cpf: form.cpf,
      gender: form.gender,
      email: form.email,
    }), {
      where: {
        id: req.params.id
      },
    });
    
    return res
      .status(200)
      .json(user.dataValues);
  } catch (error) {
    return res
      .status(422)
      .send({ error: "Houve um error." });
  }
};

export async function login(req: any, res: any) {
  const {
    email,
    password,
  } = req.body;
  
  try {
    const user = await User
      .findOne({ where: { email } });
  
    if (!user) {
      return res
        .status(404)
        .send({ error: "Email não cadastrado." });
    }

    const isAuthenticated = jwtAuthenticated
      .user(user.password, password);

    if (!isAuthenticated) {
      return res
        .status(422)
        .json({
          error: "Email ou senha inválido.",
        });
    }

    res
      .status(200)
      .json({
        id: user.id,
        token: jwtSession.session(user),
      });
  
    return res
      .status(200)
      .json({});
    } catch (error) {
      return res
        .status(422)
        .send({ error: "Houve um error." });
    }
};
